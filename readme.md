### Étape 1 : Déploiement d'un Pod

1.  **Créer un fichier de configuration pour le Pod (`pod.yaml`) :**
    
    yaml
    
    ```yaml
    apiVersion: v1
    kind: Pod
    metadata:
      name: my-app-pod
    spec:
      containers:
      - name: my-app-container
        image: registry.cluster.wik.cloud/public/echo
        ports:
        - containerPort: 8080
    ```
    
2.  **Déployer le Pod :**
    
    bash
    
    ```bash
    kubectl apply -f pod.yaml
    ```
    
3.  **Configurer le Port Forwarding pour les tests locaux :**
    
    bash
    
    ```bash
    kubectl port-forward my-app-pod 8080:8080
    ```
    
4.  **Tester le Pod localement :**
    
    bash
    
    ```bash
    curl localhost:8080/ping
    ```
    

### Étape 2 : Création d'un ReplicaSet

1.  **Modifier le fichier `pod.yaml` pour un ReplicaSet :**
    
    yaml
    
    ```yaml
    apiVersion: apps/v1
    kind: ReplicaSet
    metadata:
      name: my-app-replicaset
    spec:
      replicas: 4
      selector:
        matchLabels:
          app: my-app
      template:
        metadata:
          labels:
            app: my-app
        spec:
          containers:
          - name: my-app-container
            image: registry.cluster.wik.cloud/public/echo
            ports:
            - containerPort: 8080
    ```
    
2.  **Déployer le ReplicaSet :**
    
    bash
    
    ```bash
    kubectl apply -f pod.yaml
    ```
    
3.  **Vérifier le déploiement du ReplicaSet :**
    
    bash
    
    ```bash
    kubectl get pods
    ```
    

### Étape 3 : Mise en place d'un Service

1.  **Créer un fichier de configuration pour le Service (Modifier `pod.yaml`) :**
    
    yaml
    
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: my-app-service
    spec:
      selector:
        app: my-app
      ports:
        - protocol: TCP
          port: 8080
          targetPort: 8080
      type: ClusterIP
    ```
    
2.  **Déployer le Service :**
    
    bash
    
    ```bash
    kubectl apply -f pod.yaml
    ```
    
3.  **Tester le Service localement :**
    
    bash
    
    ```bash
    curl localhost:8080/ping
    ```
    
4.  **Activer le plugin Ingress NGINX sur Minikube :**
    
    bash
    
    ```bash
    minikube addons enable ingress
    ```
    

### Étape 4 : Configuration de l'Ingress

1.  **Créer un fichier de configuration pour l'Ingress (`Ingress.yaml`) :**
    
    yaml
    
    ```yaml
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: my-app-ingress
    spec:
      rules:
      - host: mon-domaine.com
        http:
          paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: my-app-service
                port:
                  number: 8080
    ```
    
2.  **Déployer l'Ingress :**
    
    bash
    
    ```bash
    kubectl apply -f Ingress.yaml
    ```
    
3.  **Associer le nom de domaine dans `/etc/hosts` :**
    
    bash
    
    ```bash
    sudo echo "127.0.0.1 mon-domaine.com" >> /etc/hosts
    ```
    
4.  **Tester l'Ingress localement :**
    
    bash
    
    ```bash
    curl localhost:8080/ping
    ```
    ![Screenshot 2023-11-18 at 19-44-25 Screenshot](https://hackmd.io/_uploads/SJdpOcdN6.png)
